﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using USRPLibrary;

namespace USRPLibrary
{
    public enum ErrorCodes
    {
        SomeError = 0,
        AnotherError
    }

    public class UsrpReceiver : IUsrpReceiver
    {
        public event EventHandler<byte[]> OnReadByte = (object sender, byte[] data) => { };
        public event EventHandler<int[]> OnRead = (object sender, int[] data) => { };
        public event EventHandler<byte[]> OnWriteByte = (object sender, byte[] data) => { };
        public event EventHandler<GetSpectrumEvent> OnGetSpectrum = (object sender, GetSpectrumEvent data) => { };
        public event EventHandler<ErrorCodes> OnError = (object sender, ErrorCodes data) => { };
        public event EventHandler<string> OnMessage = (object sender, string data) => { };
        public event EventHandler OnDisconnect = (object sender, EventArgs data) => { };
        public event EventHandler OnConnect = (object sender, EventArgs data) => { };

        public int SenderAddress { get; set; }
        public int ReceiverAddress { get; set; }

        private static int numberS = 0;
        int ScanIndex = 0;
        private UdpClient _udpClient = null;
        private IPEndPoint _localIpEndPoint;
        private IPEndPoint _remoteIpEndPoint;
        private Thread _threadRead;
        private int[] _currentpocketInfpart = new int[5];
        private int[] _currentpocketServpart = new int[5];
        List <int> currentpocketList = new List<int>();


        public bool Connect(string ipAddressLocal, int portLocal, string ipAddressRemote, int portRemote)
        {
            if (_udpClient != null)
            {
                _udpClient.Close();
                _udpClient = null;
            }

            if (_remoteIpEndPoint != null)
                _remoteIpEndPoint = null;

            if (_threadRead != null)
            {
                _threadRead.Abort();
                _threadRead.Join(500);
                _threadRead = null;
            }

            try
            {
                _localIpEndPoint = new IPEndPoint(IPAddress.Parse(ipAddressLocal), portLocal);
                _remoteIpEndPoint = new IPEndPoint(IPAddress.Parse(ipAddressRemote), portRemote);
                _udpClient = new UdpClient(_localIpEndPoint);

                _threadRead = new Thread(new ThreadStart(ReceiveMessage));
                _threadRead.IsBackground = true;
                _threadRead.Start();

                OnConnect?.Invoke(this, EventArgs.Empty);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine($"ошибка в Connect: { e.ToString()}");
                OnError?.Invoke(this, ErrorCodes.SomeError);
                return false;
            }

        }
        public void Disconnect()
        {
            try
            {
                if (_udpClient != null)
                {
                    _udpClient.Close();
                    _udpClient = null;
                }

                if (_threadRead != null)
                {
                    _threadRead.Abort();
                    _threadRead.Join(500);
                    _threadRead = null;
                }
                OnDisconnect?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка в Disconnect: {e.ToString()}");
            }
        }
        public void RequestSpectrum(short frequency, byte amplifier)
        {
            byte count = 0;
            try
            {
                byte code = 1;
                short lenght = 5;

                List<byte> list = new List<byte>(11);
                list.Add(1);
                list.Add(2);
                list.Add(code);
                list.Add(count);
                byte[] a = BitConverter.GetBytes(lenght);
                Array.Reverse(a);
                list.AddRange(a.ToList());

                a = BitConverter.GetBytes(frequency);
                Array.Reverse(a);
                list.AddRange(a.ToList());
                list.Add(amplifier);

                _udpClient.Send(list.ToArray<byte>(), list.ToArray<byte>().Length, _remoteIpEndPoint);
                OnWriteByte?.Invoke(this, list.ToArray());
                count++;
                numberS++;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка в Request Spectrum{ex.Message}");
                OnError?.Invoke(this, ErrorCodes.SomeError);
            }
        }

        private byte Parse(byte[] array, int number)
        {
            byte a = array[number];
            return a;
        }
        private void ReceiveMessage()
        {

            while (true)
            {
                try
                {
                    byte[] bRead = _udpClient.Receive(ref _localIpEndPoint);
                    if (bRead.Length != 8204 || bRead.Length == 0)
                        throw new Exception("The length of received pocket isn't right.");
                    OnReadByte?.Invoke(this, bRead);

                   /// DecodeInformPart(bRead);
                  ///  DecodeServicePart(bRead);
                    currentpocketList.AddRange(DecodeInformPart(bRead));
                    currentpocketList.AddRange(DecodeServicePart(bRead));
                    OnRead?.Invoke(this, currentpocketList.ToArray());
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error during receive message : {ex.Message}");
                }
            }
        }

        private int[] DecodeInformPart(byte[] bRead)
        {
            
            int counter = 0;

            int code = 0;
            int fieldlength = 0;
            byte[] informationfieldlength = new byte[2];
            for (int i = 0; i <= 4; i++)
            {
                switch (i)
                {
                    case 0:
                        SenderAddress = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 1:
                        ReceiverAddress = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 2:
                        code = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 3:
                        counter = Convert.ToInt16(Parse(bRead, i));
                        ScanIndex = counter;
                        break;
                    case 4:
                        informationfieldlength[0] = Parse(bRead, i + 1);
                        informationfieldlength[1] = Parse(bRead, i);
                        fieldlength = BitConverter.ToInt16(informationfieldlength, 0);
                        int a5 = fieldlength;
                        break;
                    default:
                        break;
                }
            }
            _currentpocketInfpart[0] = SenderAddress;
            _currentpocketInfpart[1] = ReceiverAddress;
            _currentpocketInfpart[2] = code;
            _currentpocketInfpart[3] = counter;
            _currentpocketInfpart[4] = fieldlength;
            return _currentpocketInfpart;
        }
        private List <int> DecodeServicePart(byte[] bRead)
        {
           
            int amplify = 0;
            int prevpocketquantity = 0;
            int errorcode = 0;
            int overallpocketquantity = 0;
            int currentpocketnumber = 0;
            int freq = 0;
            byte[] bytespectr1 = new byte[4096];
            byte[] bytespectr2 = new byte[4096];
            List<short> spectrum = new List<short> ();
            int[] spectrint = new int[4000];
            List<int> currentpocketServpartlist = new List<int>();
            for (int i = 6; i <= 14; i++)
            {
                switch (i)
                {
                    case 6:
                        errorcode = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 7:

                        overallpocketquantity = Parse(bRead, i);

                        if (overallpocketquantity <= prevpocketquantity)
                        {
                            OnError?.Invoke(this, ErrorCodes.AnotherError);
                        }
                        prevpocketquantity = overallpocketquantity;
                        break;
                    case 8:
                        currentpocketnumber = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 9:
                        //byte[] b = new byte[2];
                        //b[1] = Parse(bRead, i);
                        //i++;
                        //b[0] = Parse(bRead, i);//////где то здесь ошибка
                        freq = BitConverter.ToInt16(bRead, i);/////или здесь
                        int a4 = freq;
                        break;
                    case 11:
                        amplify = Convert.ToInt16(Parse(bRead, i));
                        break;
                    case 12:
                        byte[] b1 = new byte[2];
                        int k = 0;

                        //short[] spectrum = null;
                        for (int j = 0; j < (bRead.Length - 12) / 2; j++)
                        {
                            bytespectr1[j] = Parse(bRead, j + 12);
                        }
                        for (int j = 0; j < (bRead.Length - 12) / 2; j++)
                        {
                            bytespectr2[j] = Parse(bRead, j + 12 + (bRead.Length - 12) / 2);
                        }
                        for (int j = 0; j < 4000; j++)
                        {
                            b1[1] = bytespectr1[j];
                            b1[0] = bytespectr2[j];
                            spectrint[j] = BitConverter.ToInt16(b1, 0);
                            spectrum.Add((short)spectrint[j]);
                        }
                        for (int j = 0; j < 2000; j++)
                        {
                            short a = spectrum[j];
                            spectrum[j] = spectrum[j + 2000];
                            spectrum[j + 2000] = a;
                        }
                        int a2 = spectrum.Count();
                        OnGetSpectrum?.Invoke(this, new GetSpectrumEvent(spectrum.ToArray(), ScanIndex));
                        break;
                    default:
                        break;
                }
            }

            _currentpocketServpart[0] = errorcode;
            _currentpocketServpart[1] = overallpocketquantity;
            _currentpocketServpart[2] = currentpocketnumber;
            _currentpocketServpart[3] = freq;
            _currentpocketServpart[4] = amplify;
            currentpocketServpartlist.AddRange(_currentpocketServpart);
            currentpocketServpartlist.AddRange(spectrint);
            return currentpocketServpartlist;
            
        }
    }
}
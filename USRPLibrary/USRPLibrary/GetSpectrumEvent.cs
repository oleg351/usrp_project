﻿using System;

namespace USRPLibrary
{
    public class GetSpectrumEvent: EventArgs
    {
            public short[] ShortData { get; set; }
            public int IntData { get; set; }
            public GetSpectrumEvent(short[] shortdata, int intdata)
            {
                ShortData = shortdata;
                IntData = IntData;
            }
        
    }
}
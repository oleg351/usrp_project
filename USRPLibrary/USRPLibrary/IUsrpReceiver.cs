﻿using System;


namespace USRPLibrary
{
    public interface IUsrpReceiver
    {
        int SenderAddress { get; set; }
        int ReceiverAddress { get; set; }

        bool Connect(string ipAddressLocal, int portLocal, string ipAddressRemote, int portRemote);
        void Disconnect();
        void RequestSpectrum(short frequency, byte amplifier);

        event EventHandler OnConnect;
        event EventHandler OnDisconnect;
        event EventHandler<byte[]> OnReadByte;
        event EventHandler<byte[]> OnWriteByte;
        event EventHandler<ErrorCodes> OnError;
        event EventHandler<int[]> OnRead;
        event EventHandler<GetSpectrumEvent> OnGetSpectrum;
        event EventHandler<string> OnMessage;
    }
}
